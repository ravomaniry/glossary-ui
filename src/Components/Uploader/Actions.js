import React from 'react';
import Button from '@material-ui/core/Button';
import UploadIcon from '@material-ui/icons/FileUpload';
import CircleProgress from '@material-ui/core/CircularProgress';

const Actions = ({ file, uploadHandler, uploading, error, message, uploaded }) => (
    <div className='upload-actions'>
        {
            file
                ?
                (
                    <React.Fragment>
                        <div className='filename-cont'>{'File: ' + file.name || ''}</div>
                        <div className='uplad-btn-cont'>
                            {
                                uploading
                                    ?
                                    <CircleProgress />
                                    :
                                    (
                                        <Button
                                            variant="contained" color='primary'
                                            onClick={uploadHandler}
                                        >
                                            GLOSSARY XLSX UPLOAD <UploadIcon className='upload-icon' />
                                        </Button>
                                    )
                            }
                        </div>
                    </React.Fragment>
                )
                :
                (
                    <div className='no_file-message'>No file choosen</div>
                )
        }
        <div className='glossary-upload-status'>
            {message && <div className='upload-message'>{message}</div>}
            {error && <div className='upload-error'>{error}</div>}
            {
                uploaded && uploaded[0] &&
                (
                    <div className='upload-fileslist'>
                        <h3>Uploaded files:</h3>
                        {
                            uploaded.map((filename, index) => (
                                <div key={'uploaded_rile-' + index} className='upload-fileslist-item'>
                                    {filename}
                                </div>
                            ))
                        }
                    </div>
                )
            }
        </div>
    </div>
)

export default Actions;
