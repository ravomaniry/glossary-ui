import React from 'react';
import ReactDropZone from 'react-dropzone';
import Button from '@material-ui/core/Button';
import FolderIcon from '@material-ui/icons/Folder';

const DropZone = ({ onDrop }) => (
    <div className='dropzonz-cont'>
        <ReactDropZone
            className='glossary-dropzone'
            accept='application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            acceptClassName='file-accepted'
            rejectClassName='file-rejected'
            activeClassName='active-dropzone'
            onDrop={onDrop}
            multiple={false}
        >
            <p>Drop xlsx file here</p>
            <p>or</p>
            <p>
                <Button variant='contained'>
                    CLICK TO SELECT <FolderIcon />
                </Button>
            </p>
        </ReactDropZone>
    </div>
);

export default DropZone;
