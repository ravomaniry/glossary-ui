import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';

import DropZone from './DropZone';
import Actions from './Actions';
import './index.css';

class Uploader extends Component {
    state = {
        file: null,
        error: null,
        message: null,
        uploading: false,
        uploaded: [],
    }

    onDrop(acceptetFiles, rejectedfiles) {
        if (acceptetFiles[0]) this.setState({
            file: acceptetFiles[0],
            error: null,
            message: null
        });
    }

    doneHandler(filename) {
        const { uploaded } = this.state;
        this.setState({
            file: null,
            error: null,
            message: 'Suceesfuly uploaded: ' + filename,
            uploading: false,
            uploaded: [...uploaded, filename],
        });
    }

    errorHandler(error) {
        this.setState({ error });
    }

    uploadHandler() {
        const { file } = this.state;
        const saveError = this.errorHandler.bind(this);
        const doneHandler = this.doneHandler.bind(this);
        const self = this;
        if (file) {
            self.setState({ uploading: true });
            var headers = new Headers();
            var formData = new FormData();
            formData.append('file', file, file.name);

            fetch('http://localhost:8080/', {
                method: 'POST',
                body: formData,
                headers: headers
            })
                .then(res => {
                    doneHandler(file.name);
                })
                .catch(err => {
                    saveError(err);
                })
        }
    }

    render() {
        const { uploading } = this.state;
        return (
            <div className='uploader-cont'>
                <DropZone onDrop={this.onDrop.bind(this)} disabled={uploading} />
                <Actions {...this.state} uploadHandler={this.uploadHandler.bind(this)} />
            </div>
        );
    }
}

export default Uploader;
