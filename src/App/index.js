import React from 'react';

import Header from './Header';
import Uploader from '../Components/Uploader';
import './index.css';

const App = () => (
    <div className='glossary-main-cont'>
        <Header />
        <Uploader />
    </div>
);


export default App;
